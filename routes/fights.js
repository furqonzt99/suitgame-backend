var express = require("express");
const FightController = require("../controllers/fightController");
var router = express.Router();

router.post("/:code", FightController.fight);

module.exports = router;
