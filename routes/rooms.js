var express = require("express");
const RoomController = require("../controllers/roomController");
var router = express.Router();

router.post("/create", RoomController.create);

module.exports = router;
