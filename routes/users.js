var express = require('express');
const AuthController = require('../controllers/authController');
const UserController = require('../controllers/userController');
var router = express.Router();

router.post('/register', AuthController.register);
router.post('/login', AuthController.login);
router.post('/users', UserController.create);
router.get('/users', UserController.readAll);
router.get('/users/:userId', UserController.read);
router.put('/users/:userId', UserController.update);
router.delete('/users/:userId', UserController.delete);

module.exports = router;
