const { User, BiodataUser, History } = require('../models');

const UserController = {
  async create(req, res) {
    const { username, password, name, email, bornDate } = req.body;
    const data = await User.create(
      {
        username: username,
        password: password,
        BiodataUser: {
          name: name,
          email: email,
          bornDate: bornDate,
        },
      },
      {
        include: BiodataUser,
      }
    );

    return res.json(data);
  },
  async readAll(req, res) {
    let data;

    data = await User.findAll({
      attributes: {
        exclude: ['password'],
      },
      include: [
        {
          model: BiodataUser,
        },
        {
          model: History,
        },
      ],
    });

    return res.json(data);
  },
  async read(req, res) {
    let data;

    data = await User.findOne({
      where: {
        id: req.params.id,
      },
      attributes: {
        exclude: ['password'],
      },
      include: [
        {
          model: BiodataUser,
        },
        {
          model: History,
        },
      ],
    });

    return res.json(data);
  },
  async update(req, res) {
    const { name, email, bornDate } = req.body;

    const data = await BiodataUser.update(
      {
        name: name,
        email: email,
        bornDate: bornDate,
      },
      {
        where: {
          userId: req.params.userId,
        },
      }
    );

    return res.json(data);
  },
  async delete(req, res) {
    const data = await User.destroy({
      where: {
        id: req.params.id,
      },
    });

    return res.json(data);
  },
};

module.exports = UserController;
