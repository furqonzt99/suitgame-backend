const { User } = require('../models');

const Validator = require('fastest-validator');

const v = new Validator();

const AuthController = {
  login(req, res) {
    const schema = {
      username: { type: 'string', min: 5, max: 255 },
      password: { type: 'string', min: 8, max: 255 },
    };

    const validate = v.validate(req.body, schema);

    const meta = {
      code: 400,
      status: 'error',
      message: 'bad request',
    };

    if (validate.length) {
      res.formatter.badRequest(validate, meta);
    } else {
      User.authenticate(req.body)
        .then((user) => {
          console.log(user.id);
          const data = {
            id: user.id,
            username: user.username,
            accessToken: user.generateToken(),
          };

          res.formatter.ok(data);
        })
        .catch((err) => {
          res.formatter.serverError(err, 'error');
        });
    }
  },
  register(req, res) {
    const schema = {
      username: { type: 'string', min: 5, max: 255 },
      name: { type: 'string', min: 3, max: 255 },
      password: { type: 'string', min: 8, max: 255 },
      email: { type: 'email', require },
      bornDate: { type: 'string', require },
    };

    const validate = v.validate(req.body, schema);

    const meta = {
      code: 400,
      status: 'error',
      message: 'bad request',
    };

    if (validate.length) {
      res.formatter.badRequest(validate, meta);
    } else {
      User.register(req.body)
        .then(() => {
          const meta = {
            code: 200,
            message: 'success',
          };
          res.formatter.ok(meta);
        })
        .catch((err) => {
          const meta = {
            code: 400,
            message: 'Bad Request',
          };
          res.formatter.serverError(meta);
        });
    }
  },
};

module.exports = AuthController;
