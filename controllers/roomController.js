const { Room } = require('../models');

const Validator = require('fastest-validator');

const v = new Validator();

async function generateCode() {
  function randomCode() {
    return (
      Math.random().toString(36).substring(2, 5) +
      Math.random().toString(36).substring(5, 8)
    );
  }

  const code = randomCode();

  const check = await Room.findAll({
    where: {
      code: randomCode(),
    },
  });

  if (check.length) {
    return generateCode();
  }

  return code;
}

const RoomsController = {
  async create(req, res) {
    const schema = {
      name: { type: 'string', min: 5, max: 255 },
    };

    const validate = v.validate(req.body, schema);

    const meta = {
      code: 400,
      status: 'error',
      message: 'bad request',
    };

    if (validate.length) {
      res.formatter.badRequest(validate, meta);
    } else {
      const data = await Room.create({
        name: req.body.name,
        code: await generateCode(),
      });

      res.formatter.ok(data);
    }
  },
};

module.exports = RoomsController;
