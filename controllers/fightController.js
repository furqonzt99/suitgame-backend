const { History, Room } = require('../models');
const { Op } = require('sequelize');

async function getFightResult(id, code, round) {
  const p1 = await History.findAll({
    where: {
      userId: id,
      roomCode: code,
      round: round,
    },
  });

  const p2 = await History.findAll({
    where: {
      roomCode: code,
      round: round,
      [Op.not]: {
        userId: id,
      },
    },
  });

  const p1Suit = p1[0].suit;
  let p2Suit = null;
  let p2Id = null;

  if (p2.length > 0) {
    p2Suit = p2[0].suit;
    p2Id = p2[0].userId;
  }

  const data = {
    round: round,
    you: {
      id: p1[0].userId,
      suit: p1Suit,
    },
    opponent: {
      id: p2Id,
      suit: p2Suit,
    },
    result:
      p2Suit == null
        ? 'Wait for another player!'
        : makeDecision(p1Suit, p2Suit),
  };

  return data;
}

function makeDecision(p1, p2) {
  if (p1 == p2) {
    return 'DRAW';
  }

  let res;

  if (p1 == 'R' && p2 == 'S') {
    res = 'WIN';
  } else if (p1 == 'P' && p2 == 'R') {
    res = 'WIN';
  } else if (p1 == 'S' && p2 == 'P') {
    res = 'WIN';
  } else {
    res = 'LOSE';
  }

  return res;
}

function summaryDecision(data) {
  let point = 0;
  let res = null;

  data.forEach((e) => {
    if (e.result == 'WIN') {
      point++;
    } else if (e.result == 'LOSE') {
      point--;
    }
  });

  if (point == 0) {
    res = 'DRAW';
  } else if (point > 0) {
    res = 'WIN';
  } else {
    res = 'LOSE';
  }

  return res;
}

const FightController = {
  async fight(req, res) {
    // cek input round
    if (req.body.round > 3) {
      const err = 'Maximum round is 3!';

      const meta = {
        code: 400,
        status: 'Bad Request',
      };

      res.formatter.badRequest(err, meta);
    }

    // cek room code
    const isRoomValid = await Room.findOne({
      where: {
        code: req.params.code,
      },
    });

    if (!isRoomValid) {
      const err = 'Room code is not exist!';

      const meta = {
        code: 404,
        status: 'Not Found',
      };

      res.formatter.notFound(err, meta);
    }

    // cek player has suit
    const isPlayerHasSuit = await History.findOne({
      where: {
        userId: req.user.id,
        roomCode: req.params.code,
        round: req.body.round,
      },
    });

    if (!isPlayerHasSuit) {
      await History.create({
        userId: req.user.id,
        roomCode: req.params.code,
        round: req.body.round,
        suit: req.body.suit,
      });
    }

    let data = [];

    // push every game data
    for (let i = 1; i <= req.body.round; i++) {
      data.push(await getFightResult(req.user.id, req.params.code, i));
    }

    if (data.length < 3) {
      // go to next round
      data.push({
        summary: 'Go to next round!',
      });
    } else {
      // push game summary
      data.push({
        summary: summaryDecision(data),
      });
    }

    return res.json(data);
  },
};

module.exports = FightController;
