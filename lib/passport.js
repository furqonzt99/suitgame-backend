require("dotenv").config();

const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { User } = require("../models");

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken("authorization"),
  secretOrKey: process.env.SECRET_KEY_JWT,
};

passport.use(
  new JwtStrategy(options, async (payload, done) => {
    User.findOne({
      where: {
        username: payload.username,
      },
    })
      .then((user) => done(null, user))
      .catch((err) => done(err, false));
  })
);

module.exports = passport;
