"use strict";

const { DataTypes } = require("sequelize/dist");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("Users", "role", { type: DataTypes.STRING });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn("Users", "roleId");
  },
};
