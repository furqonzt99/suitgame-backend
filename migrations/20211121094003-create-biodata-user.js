'use strict';

const { DataTypes } = require('sequelize');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      'BiodataUsers',
      {
        id: {
          allowNull: false,
          primaryKey: true,
          defaultValue: DataTypes.UUIDV4,
          type: DataTypes.UUID,
        },
        userId: {
          type: DataTypes.UUID,
        },
        name: {
          type: Sequelize.STRING,
        },
        bornDate: {
          type: Sequelize.DATEONLY,
        },
        email: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      },
      {
        indexes: [
          {
            unique: true,
            fields: ['email'],
          },
        ],
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('BiodataUsers');
  },
};
