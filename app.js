const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { responseEnhancer } = require('express-response-formatter');
const passport = require('passport');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const roomsRouter = require('./routes/rooms');
const fightsRouter = require('./routes/fights');
const authMiddleware = require('./middlewares/authMiddleware');

const swaggerJSON = require('./suitgame.json');
const swaggerUI = require('swagger-ui-express');

const app = express();

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Add formatter functions to "res" object via "responseEnhancer()"
app.use(responseEnhancer());

app.use(passport.initialize());

app.use('/', usersRouter);
app.use('/rooms', authMiddleware, roomsRouter);
app.use('/fights', authMiddleware, fightsRouter);

module.exports = app;
