'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BiodataUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'userId',
      });
    }
  }
  BiodataUser.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      userId: DataTypes.UUID,
      name: DataTypes.STRING,
      bornDate: DataTypes.DATE,
      email: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'BiodataUser',
    }
  );
  return BiodataUser;
};
