'use strict';
require('dotenv').config();

const { Model } = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.BiodataUser, {
        foreignKey: 'userId',
      });
      this.hasMany(models.History, {
        foreignKey: 'userId',
      });
    }

    //method untuk hash password
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    //method register
    static register = ({ username, password, name, email, bornDate }) => {
      //hash password
      const encryptedPassword = this.#encrypt(password);

      //default Role
      const defaultRole = 'PlayerUser';

      const { BiodataUser } = require('./biodatauser');

      //buat data di database dengan password terenkripsi
      return this.create(
        {
          username,
          password: encryptedPassword,
          role: defaultRole,
          BiodataUser: {
            name: name,
            email: email,
            bornDate: bornDate,
          },
        },
        {
          include: BiodataUser,
        }
      );
    };

    //method cek password
    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      };

      const token = jwt.sign(payload, process.env.SECRET_KEY_JWT);

      return token;
    };

    //method authentikasi
    static authenticate = async ({ username, password }) => {
      try {
        //cari user berdasarkan username
        const user = await this.findOne({ where: { username } });

        //jika username tidak ketemu
        if (!user) {
          return Promise.reject('User not found!');
        }

        //cek password
        const isPasswordValid = user.checkPassword(password);

        //jika password tidak sesuai atau salah
        if (!isPasswordValid) {
          return Promise.reject('Password Invalid!');
        }

        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  User.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      role: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'User',
    }
  );
  return User;
};
