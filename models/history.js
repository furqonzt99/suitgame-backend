'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        foreignKey: 'userId',
      });
    }
  }
  History.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      roomCode: DataTypes.STRING,
      userId: DataTypes.UUID,
      suit: DataTypes.STRING,
      round: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'History',
    }
  );
  return History;
};
