import { useState } from 'react';
import Navbar from '../components/Navbar';
import Header from '../components/Header';

function CreatePlayer() {
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [bornDate, setBornDate] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(name);
    console.log(username);
    console.log(email);
    console.log(password);
    console.log(bornDate);
  };

  return (
    <>
      <Header />
      <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-3">
        <h2>Create User</h2>

        <form className="mt-5" onSubmit={handleSubmit}>
          <div className="mb-3">
            <label className="form-label">Username</label>
            <input
              type="text"
              className="form-control"
              name="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              required
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input
              type="password"
              className="form-control"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Name</label>
            <input
              type="text"
              className="form-control"
              name="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              required
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Email address</label>
            <input
              type="email"
              className="form-control"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Born Date</label>
            <input
              type="date"
              className="form-control"
              name="bornDate"
              value={bornDate}
              onChange={(e) => setBornDate(e.target.value)}
              required
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Create User
          </button>
        </form>
      </main>

      <div className="container-fluid">
        <div className="row">
          <Navbar />
        </div>
      </div>
    </>
  );
}

export default CreatePlayer;
