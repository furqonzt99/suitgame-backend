import Navbar from '../components/Navbar';
import Header from '../components/Header';

function IndexPlayer() {
  return (
    <>
      <Header />
      <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-3">
        <div class="row">
          <div class="col">
            <h2>All User</h2>
          </div>
          <div class="col">
            <a href="/create" class="btn btn-primary float-end">
              Create User
            </a>
          </div>
        </div>

        <div class="table-responsive mt-3">
          <table class="table table-striped table-lg">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Name</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>username</td>
                <td>name</td>
                <td>
                  <a href="/edit" id="btnEdit" class="btn btn-sm btn-warning">
                    Edit
                  </a>
                  <a href="/#" class="btn btn-sm btn-danger ms-3">
                    Delete
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </main>

      <div className="container-fluid">
        <div className="row">
          <Navbar />
        </div>
      </div>
    </>
  );
}

export default IndexPlayer;
