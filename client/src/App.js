import './styles/bootstrap.min.css';
import './styles/dashboard.css';
import { Routes, Route } from 'react-router-dom';
import IndexPlayer from './pages/IndexPlayer';
import CreatePlayer from './pages/CreatePlayer';
import EditPlayer from './pages/EditPlayer';

function App() {
  return (
    <Routes>
      <Route path="/" element={<IndexPlayer />} />
      <Route path="/create" element={<CreatePlayer />} />
      <Route path="/edit" element={<EditPlayer />} />
    </Routes>
  );
}

export default App;
