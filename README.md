## Suit Game

API and Client UI for play suit game

# Start Backend :

To start the backend API, run the command below

First:

```
npm install && npm update
```

Second, migrate and seed:
Please create database with postgresql & setup the config file before run this command.

```
npx sequelize-cli db:migrate
```

```
npx sequelize-cli db:seed:all
```

Last but not least, start the server:

```
npm run dev
```

To see the Open API documentation, go to this path

```
/docs
```

# Start Frontend :

To start the frontenf UI, run the command below

First:

```
cd client
```

Second:

```
npm install && npm update
```

Second:

```
npm start
```

This project is under development, so be patient if you have an error :)
